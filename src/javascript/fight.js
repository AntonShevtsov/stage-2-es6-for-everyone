import PopUpView from './popUpView';

let fightersForBattle = [];
let fightSequence;

function fight(fighter1, fighter2) {

    fightSequence = setInterval( () => {
        const damageByFighter1 = _calculateDamage(fighter1, fighter2);
        const healthBar2 = document.getElementById('fighter2');
        fighter2.health = fighter2.health - damageByFighter1;
        healthBar2.value = fighter2.health;

        const damageByFighter2 = _calculateDamage(fighter2, fighter1);
        const healthBar1 = document.getElementById('fighter1');
        fighter1.health = fighter1.health - damageByFighter2;
        healthBar1.value = fighter1.health;

        determineWinner(fighter1, fighter2);
    }, 500);

function determineWinner(fighter1, fighter2) {
    if (fighter1.health <= 0) {
        _displayPopUp(`Player2 ${fighter2.name} is the WINNER`)
        stopFightSequence();
        return
    }
    if (fighter2.health <= 0) {
        _displayPopUp(`Player1 ${fighter1.name} is the WINNER`)
        stopFightSequence();
        return
    }
    return
}

}

function stopFightSequence() {
    clearInterval(fightSequence);
    fightersForBattle = [];
}

function _calculateDamage(attacker, defender) {
    const damage = attacker.getHitPower() - defender.getBlockPower();

    if (damage > 0) {
        return damage
    }
    return 0
}

function _displayPopUp(title) {
    const root = document.getElementById('root');
    const popup = new PopUpView(title);
    root.append(popup.element);

    setTimeout(_removePopUp, 6000);
}

function _removePopUp() {
    const popUp = document.getElementById('popup');
    if(popup) {
      const root = document.getElementById('root');
      root.removeChild(popUp);
    }
  }


export { fight, fightersForBattle, stopFightSequence };