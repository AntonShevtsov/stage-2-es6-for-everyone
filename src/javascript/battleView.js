import View from './view';
import { stopFightSequence } from './fight';

class BattleView extends View {
    constructor(fightersArray) {
    super();
    
    this.createDialogWindow(fightersArray);
  }

  createDialogWindow(fightersArray) {

    this.element = this.createElement({tagName: 'div', className: 'battle__container'})
    this.element.addEventListener('click', event => this.handleCloseClick(event), false);

    const dialogContent = this.createElement ({ tagName: 'div', className: 'battle__content' });

    const closeButton = this.createElement( {tagName: 'span', className: 'battle__close'} );
    closeButton.innerText = '×';
    closeButton.addEventListener('click', event => this.handleCloseClick(event), false);
    dialogContent.append(closeButton);
    
    fightersArray.forEach( (fighter, index) => {
        const { name, health, source } = fighter;
        const nameElement = this.createName(name);
        const imageElement = this.createImage(source);
        const healthElement = this.createHealth(health, index);
        
        const detailsContainer = this.createElement({ tagName: 'div', className: 'dialog__details' });
        detailsContainer.append(nameElement, healthElement);
    
        const fighterContainer = this.createElement ({ tagName: 'div', className: 'fighter__container' });
        fighterContainer.append(imageElement, detailsContainer);

        dialogContent.append(fighterContainer);
    });

    this.element.append(dialogContent);

    return this.element
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'details__item' });
    nameElement.innerText = 'Name: ' + name;

    return nameElement;
  }

  createHealth(health, index) {
    const title = this.createElement({ tagName: 'span', className: 'details__item' });
    title.innerText = 'Health: ' + health;

    const attributes = {
        'value': `${health}`,
        'max': `${health}`,
        'low': `${health*0.5}`,
        'id': `fighter${index+1}`
    }
    const healthBar = this.createElement({ tagName: 'meter', className: 'details__health', attributes });

    const healthElement = this.createElement({ tagName: 'div', className: 'details__health-bar' });
    healthElement.append(title, healthBar);

    return healthElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  handleCloseClick(event) {
    const root = document.getElementById('root')
    const battleContainer = document.querySelector('.battle__container');
    const closeButton = document.querySelector('.battle__close');
    if (event.target === battleContainer || event.target === closeButton) {
        root.removeChild(battleContainer);
        stopFightSequence();
    }
  }
}

export default BattleView;