import View from './view';

class PopUpView extends View {
  constructor(fighter) {
    super();

    this.createPopUp(fighter);
  }

  createPopUp(title) {
    const attributes = { 'id': 'popup'}
    this.element = this.createElement({ tagName: 'div', className: 'popup', attributes });
    this.element.innerText = title;

    return this.element
  }
}

export default PopUpView;
