import View from './view';

class DetailsView extends View {
  constructor(fighterDetails) {
    super();
    
    this.createDialogWindow(fighterDetails);
  }

  createDialogWindow(fighterDetails) {
    const {
        name,
        health,
        attack,
        defense,
        source
    } = fighterDetails;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const healthElement = this.createHealth(health);
    const attackElelment = this.createAttack(attack);
    const defenseElement = this.createdefense(defense);
    
    const closeButton = this.createElement( {tagName: 'span', className: 'dialog__close'} );
    closeButton.innerText = '×';
    closeButton.addEventListener('click', event => this.handleCloseClick(event), false);
    
    const detailsContainer = this.createElement({ tagName: 'div', className: 'dialog__details' });
    detailsContainer.append(nameElement, healthElement, attackElelment, defenseElement, closeButton);

    const dialogContent = this.createElement ({ tagName: 'div', className: 'dialog__content' });
    dialogContent.append(imageElement, detailsContainer);

    this.element = this.createElement({tagName: 'div', className: 'dialog__container'})
    this.element.append(dialogContent);
    this.element.addEventListener('click', event => this.handleCloseClick(event), false);

    return this.element
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'details__item' });
    nameElement.innerText = 'Name: ' + name;

    return nameElement;
  }

  createHealth(health) {
    const healthElement = this.createElement({ tagName: 'span', className: 'details__item' });
    healthElement.innerText = 'Health: ' + health;

    return healthElement;
  }

  createAttack (attack) {
    const attackElement = this.createElement({ tagName: 'span', className: 'details__item' });
    attackElement.innerText = 'Attack: ' + attack;

    return attackElement;
  }

  createdefense (defense) {
    const defenseElement = this.createElement({ tagName: 'span', className: 'details__item' });
    defenseElement.innerText = 'Defense: ' + defense;

    return defenseElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  handleCloseClick(event) {
    const root = document.getElementById('root')
    const dialog = document.querySelector('.dialog__container');
    const closeButton = document.querySelector('.dialog__close');
    if (event.target == dialog || event.target == closeButton) {
      root.removeChild(dialog);
    }
  }
}

export default DetailsView;