import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import DetailsView from './detailsView';
import { fight, fightersForBattle } from './fight';
import Fighter from './fighter';
import BattleView from './battleView';

const PAGE_TITLE = 'To start the fight, please, select two fighters'

class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleClick.bind(this);
    this.createTitle(PAGE_TITLE);
    this.createFighters(fighters);
  }

  fightersDetailsMap = new Map();

  createTitle(title) {
    const root = document.getElementById('root');
    const titleElement = this.createElement({ tagName: 'h1', className: 'title' });
    titleElement.innerText = title;
    root.append(titleElement);
  }

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });

    this.element = this.createElement({ tagName: 'div', className: 'fighters' });
    this.element.append(...fighterElements);
  }

  async handleClick(event, fighter) {
    const fighterDetails = await this.getFighterDetails(fighter);

    const addButtons = document.querySelectorAll('.add-button');
    const addButtonsArray = Array.from(addButtons);
    if (addButtonsArray.includes(event.target)) {
      this.handleButtonClick(event, fighterDetails);
      return
    }

    this.handleFighterClick(event, fighterDetails);
  }

  
  handleButtonClick(event, fighterDetails) {
    const fighter = new Fighter(fighterDetails);
    fightersForBattle.push(fighter);
    if (fightersForBattle.length === 2) {
      const battleView = new BattleView(fightersForBattle);
      const root = document.getElementById('root');
      root.append(battleView.element);

      fight(...fightersForBattle);
    }
  }

  handleFighterClick(event, fighterDetails) {
    this.displayDetailsDialog(fighterDetails);
    // allow to edit health and power in this modal
  }

  async getFighterDetails(fighter) {
    const fighterId = this.checkFighterInDetailsMap(this.fightersDetailsMap, fighter);
    if (fighterId) {
      return this.fightersDetailsMap.get(fighterId);
    }

    const fighterDetails = await fighterService.getFighterDetails(fighter._id);
    this.addTofightersDetailsMap(fighter._id, fighterDetails);
    return fighterDetails
  }

  checkFighterInDetailsMap(map, fighter) {
    for (let [key, value] of map) {
      if (value._id === fighter._id) { 
        return key; 
      }
    }
  }

  addTofightersDetailsMap(id, details) {
    this.fightersDetailsMap.set(id, details);
  }

  displayDetailsDialog(fighterDetails) {
    const detailsView = new DetailsView(fighterDetails);;
    const root = document.getElementById('root');
    root.append(detailsView.element);
  }
}

export default FightersView;