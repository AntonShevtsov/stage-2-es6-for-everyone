const MIN_MULTIPLIER = 1;
const MAX_MULTIPLIER = 2;

class Fighter {
  constructor(fighterDetails) {
    Object.assign(this, fighterDetails);
  }

  getHitPower() {
    const criticalHitChance = Math.random() * (MAX_MULTIPLIER - MIN_MULTIPLIER) + MIN_MULTIPLIER;
    const hitPower = Math.floor(this.attack * criticalHitChance);
    return hitPower
  }

  getBlockPower() {
    const dodgeChance = Math.random() * (MAX_MULTIPLIER - MIN_MULTIPLIER) + MIN_MULTIPLIER;
    const blockPower = Math.floor(this.defense * dodgeChance);
    return blockPower
  }
}

export default Fighter;