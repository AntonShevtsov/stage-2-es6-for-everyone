import View from './view';

class FighterView extends View {
  constructor(fighter, handleClick) {
    super();

    this.createFighter(fighter, handleClick);
  }

  createFighter(fighter, handleClick) {
    const { name, source } = fighter;
    const nameElement = this.createName(name);
    const imageElement = this.createImage(source);
    const buttonElement = this.createAddFighterButton();

    this.element = this.createElement({ tagName: 'div', className: 'fighter' });
    this.element.append(imageElement, nameElement, buttonElement);
    this.element.addEventListener('click', event => handleClick(event, fighter), false);
  
  }

  createName(name) {
    const nameElement = this.createElement({ tagName: 'span', className: 'name' });
    nameElement.innerText = name;

    return nameElement;
  }

  createImage(source) {
    const attributes = { src: source };
    const imgElement = this.createElement({
      tagName: 'img',
      className: 'fighter-image',
      attributes
    });

    return imgElement;
  }

  createAddFighterButton() {
    const attributes = { type: 'button' }; 
    const buttonElement = this.createElement({ tagName: 'button', className: 'add-button', attributes });
    buttonElement.innerText = 'Select Fighter'

    return buttonElement;
  }
}

export default FighterView;